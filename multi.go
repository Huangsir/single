package single

import (
	"sync"
	"sync/atomic"
)

type Multi struct {
	m        sync.Mutex
	g        sync.WaitGroup
	doing    uint32
	capacity uint32
}

func (m *Multi) Cap(capacity uint32) {
	atomic.StoreUint32(&m.capacity, capacity)
}

func (m *Multi) Waitdo(fc func()) {
	for atomic.LoadUint32(&m.doing) >= m.capacity {
		m.g.Wait()
	}
	m.m.Lock()
	atomic.StoreUint32(&m.doing, m.doing+1)
	m.g.Add(1)
	m.m.Unlock()
	go func() {
		fc()
		m.m.Lock()
		atomic.StoreUint32(&m.doing, m.doing-1)
		m.g.Done()
		m.m.Unlock()
	}()
}

func (m *Multi) Do(fc func()) {
	if atomic.LoadUint32(&m.doing) >= m.capacity {
		return
	}
	m.m.Lock()
	atomic.StoreUint32(&m.doing, m.doing+1)
	m.g.Add(1)
	m.m.Unlock()
	go func() {
		fc()
		m.m.Lock()
		atomic.StoreUint32(&m.doing, m.doing-1)
		m.g.Done()
		m.m.Unlock()
	}()
}
