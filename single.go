package single

import (
	"sync"
	"sync/atomic"
)

type Single struct {
	m     sync.Mutex
	doing uint32
}

func (s *Single) Do(fc func()) {
	if atomic.LoadUint32(&s.doing) == 1 {
		return
	}
	s.m.Lock()
	defer s.m.Unlock()
	if s.doing == 0 {
		atomic.StoreUint32(&s.doing, 1)
	}
	fc()
	atomic.StoreUint32(&s.doing, 0)
}

func (s *Single) Waitdo(fc func()) {
	s.m.Lock()
	defer s.m.Unlock()
	atomic.StoreUint32(&s.doing, 1)
	defer atomic.StoreUint32(&s.doing, 0)
	fc()
}
